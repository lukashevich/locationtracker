//
//  LTLocationManger.h
//  LocationTracker
//
//  Created by Max Lukashevich on 28.07.16.
//  Copyright © 2016 Maks Lukashevich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol LTLocationMangerDelegate <NSObject>
- (void)speedChanged:(double)speed;
- (void)handleError:(NSError *)error;
@end

@class CLLocation;

@interface LTLocationManger : NSObject

@property (nonatomic, assign, readonly) BOOL updatingLocation;
@property (nonatomic, strong, readonly) NSMutableArray * locations;

@property (nonatomic, weak) id <LTLocationMangerDelegate> delegate;

+ (instancetype)sharedInstance;
- (void)startLocationUpdates;
- (void)stopLocationUpdates;
- (void)beginLocationsSaving;
- (void)stopLocationsSaving;

@end
