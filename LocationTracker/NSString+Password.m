//
//  NSString+Password.m
//  LocationTracker
//
//  Created by Max Lukashevich on 30.07.16.
//  Copyright © 2016 Maks Lukashevich. All rights reserved.
//

#import "NSString+Password.h"

@implementation NSString (Password)

+ (NSString *)generatePassword {
  NSString *alphabet  = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXZY0123456789";
  NSMutableString *string = [NSMutableString stringWithCapacity:20];
  for (NSUInteger i = 0U; i < 20; i++) {
    u_int32_t r = arc4random() % [alphabet length];
    unichar c = [alphabet characterAtIndex:r];
    [string appendFormat:@"%C", c];
  }
  return string;
}
@end
