//
//  LTLocationManger.m
//  LocationTracker
//
//  Created by Max Lukashevich on 28.07.16.
//  Copyright © 2016 Maks Lukashevich. All rights reserved.
//

#import "LTLocationManger.h"
#import "NSError+Location.h"
#import "KeychainWrapper.h"
#import "LocationTracker-Swift.h"
#import "NSString+Password.h"

@import Security;

@import CoreLocation;

NSTimeInterval const LTSavingTimerInterval = 1;

@interface LTLocationManger () <CLLocationManagerDelegate>

@property (nonatomic, assign, readwrite) BOOL updatingLocation;
@property (nonatomic, strong, readwrite) CLLocation * currentLocation;
@property (nonatomic, strong, readwrite) NSMutableArray * locations;
@property (nonatomic, strong, readwrite) NSMutableArray * currentSessionLocations;

@property (nonatomic, strong) CLLocationManager * locationManager;
@property (nonatomic, strong) NSTimer * savingTimer;
@property (nonatomic, strong) NSDate * startDate;

@property (nonatomic, copy) void (^authCompletion)();

@end

@implementation LTLocationManger

- (void)dealloc {
  [self stopLocationUpdates];
}

- (instancetype)init {
  self = [super init];
  if (self) {
    self.startDate = [NSDate date];
    self.locations = [NSMutableArray new];
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    [self requestAuthorizationIfNeeded];
  }
  return self;
}

#pragma mark - Interface

+ (instancetype)sharedInstance {
  static dispatch_once_t onceToken;
  static LTLocationManger * instance;
  dispatch_once(&onceToken, ^{
    instance = [[self alloc] init];
  });
  return instance;
}

- (void)startLocationUpdates {
  if ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedAlways) {
    __weak typeof(self) ws = self;
    self.authCompletion = ^() {
      [ws startLocationUpdates];
    };
    [self requestAuthorizationIfNeeded];
  }
  else {
    self.currentSessionLocations = [NSMutableArray new];
    [self.locations addObjectsFromArray:[self loadSavedLocations]];
    self.updatingLocation = YES;
    [self.locationManager startUpdatingLocation];
  }
}

- (void)stopLocationUpdates {
  self.updatingLocation = NO;
  [self.locationManager stopUpdatingLocation];
  [self stopLocationsSaving];
}

- (CLLocation *)currentLocation {
  if (_currentLocation) {
    if (_currentLocation.coordinate.latitude == 0.0 && _currentLocation.coordinate.longitude == 0.0) {
      _currentLocation = nil;
    }
  }
  return _currentLocation;
}


- (void)beginLocationsSaving {
  if (self.savingTimer) {
    [self.savingTimer invalidate];
  }
  [self saveCurrentLocation];
  __weak typeof(self) ws = self;
  self.savingTimer = [NSTimer scheduledTimerWithTimeInterval:LTSavingTimerInterval
                                                      target:ws
                                                    selector:@selector(saveCurrentLocation)
                                                    userInfo:nil
                                                     repeats:YES];
}

- (void)stopLocationsSaving {
  [self.savingTimer invalidate];
  self.savingTimer = nil;
  [self saveLocationsToFile];
}

- (void)saveCurrentLocation {
  CLLocation * location = [self currentLocation];
  if (location) {
    [self.locations addObject:location];
  }
}

#pragma mark - Private

- (void)requestAuthorizationIfNeeded {
  CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
  if (status == kCLAuthorizationStatusRestricted ||
      status == kCLAuthorizationStatusDenied) {
    [self.delegate handleError:[NSError authorizationFailed]];
  }
  else {
    [self.locationManager requestAlwaysAuthorization];
  }
}

- (void)saveLocationsToFile {
  NSData * data = [NSKeyedArchiver archivedDataWithRootObject:self.locations];
  NSData * encryptedData = [RNCryptor encryptData:data password:[self password]];
  [encryptedData  writeToFile:[self filePath] atomically:YES];
}

- (NSMutableArray *)loadSavedLocations {
  NSError *error = nil;
  NSData * data = [NSData dataWithContentsOfFile:[self filePath]];
  if (data == nil) {
    return nil;
  }
  NSData * decryptedData = [RNCryptor decryptData:data password:[self password] error:&error];
  if (error != nil) {
    NSLog(@"Error : %@", error);
    return nil;
  }
  NSArray * locations = [NSKeyedUnarchiver unarchiveObjectWithData:decryptedData];
  return [locations mutableCopy];
}

#pragma mark - Helpers

- (NSString *)filePath {
  NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
  NSString * documentsDirectory = [paths objectAtIndex:0];
  return [documentsDirectory stringByAppendingPathComponent:@"locations"];
}

- (NSString *)password {
  KeychainWrapper * keychain = [KeychainWrapper new];
  NSString * password = [keychain myObjectForKey:(NSString *)kSecValueData];
  if (password == nil) {
    password = [NSString generatePassword];
    [keychain mySetObject:password forKey:(NSString *)kSecValueData];
  }
  return password;
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
  CLLocation * location = [locations lastObject];
  if ([self.startDate compare:location.timestamp] == NSOrderedAscending) {
    self.currentLocation = [locations lastObject];
    [self.currentSessionLocations addObject:locations];
    double speed = self.currentLocation.speed;
    if (self.currentSessionLocations.count > 1) {
      [self.delegate speedChanged:speed >= 0 ? speed : 0];
    }
  }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
  [self.delegate handleError:error];
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
  if (status == kCLAuthorizationStatusDenied || status == kCLAuthorizationStatusRestricted) {
    [self stopLocationUpdates];
    [self.delegate handleError:[NSError authorizationFailed]];
  }
  else if (status == kCLAuthorizationStatusAuthorizedAlways) {
    if (self.authCompletion) {
      self.authCompletion();
    }
  }
}


@end
