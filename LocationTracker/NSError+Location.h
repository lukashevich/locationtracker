//
//  NSError+Location.h
//  LocationTracker
//
//  Created by Max Lukashevich on 28.07.16.
//  Copyright © 2016 Maks Lukashevich. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSError (Location)

+ (NSError *)authorizationFailed;

@end
