//
//  NSString+Password.h
//  LocationTracker
//
//  Created by Max Lukashevich on 30.07.16.
//  Copyright © 2016 Maks Lukashevich. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Password)

+ (NSString *)generatePassword;

@end
