//
//  LTMapViewController.m
//  LocationTracker
//
//  Created by Max Lukashevich on 28.07.16.
//  Copyright © 2016 Maks Lukashevich. All rights reserved.
//

#import "LTMapViewController.h"

@import MapKit;

@interface LTMapViewController () <MKMapViewDelegate>

@property (nonatomic, weak) IBOutlet MKMapView * mapView;
@property (nonatomic, strong) NSArray * locationsArray;
@end

@implementation LTMapViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  self.title = @"Map";
  UIBarButtonItem * back = [[UIBarButtonItem alloc] initWithTitle:@"back"
                                                            style:UIBarButtonItemStylePlain
                                                           target:self
                                                           action:@selector(backAction)];
  self.navigationItem.leftBarButtonItem = back;
  [self drawRoute];
}

- (void)setLocations:(NSArray *)locations {
  _locationsArray = locations;
  if (self.mapView) {
    [self drawRoute];
  }
}

#pragma mark - Private

- (void)backAction {
  [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)drawRoute {
  if (self.locationsArray.count > 2) {
    CLLocationCoordinate2D coordinates[self.locationsArray.count];
    int i = 0;
    for (CLLocation * location in self.locationsArray) {
      coordinates[i] = location.coordinate;
      i++;
    }
    MKGeodesicPolyline * polyline = [MKGeodesicPolyline polylineWithCoordinates:coordinates count:_locationsArray.count];
    [self.mapView addOverlay:polyline];
    [self.mapView setVisibleMapRect:[polyline boundingMapRect]
                        edgePadding:UIEdgeInsetsMake(10.0, 10.0, 10.0, 10.0)
                           animated:YES];
  }
}

#pragma mark - MKMapViewDelegate

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id <MKOverlay>)overlay {
  MKPolylineRenderer * renderer = [[MKPolylineRenderer alloc] initWithPolyline:overlay];
  renderer.lineWidth = 2;
  renderer.strokeColor = [UIColor blueColor];
  return renderer;
}
@end
