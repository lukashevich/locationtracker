//
//  NSError+Location.m
//  LocationTracker
//
//  Created by Max Lukashevich on 28.07.16.
//  Copyright © 2016 Maks Lukashevich. All rights reserved.
//

#import "NSError+Location.h"

@implementation NSError (Location)

+ (NSError *)authorizationFailed {
  return [self errorWithDescription:@"Location track do not have permissions to track your location"
                             reason:@"Please change permission in system settings and try again"];
}

+ (NSError *)errorWithDescription:(NSString *)description reason:(NSString *)reason {
  return [NSError errorWithDomain:@"location.tracker"
                             code:0
                         userInfo:@{NSLocalizedDescriptionKey : description, NSLocalizedFailureReasonErrorKey : reason}];
}
@end
