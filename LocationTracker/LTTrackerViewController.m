//
//  LTTrackerViewController.m
//  LocationTracker
//
//  Created by Max Lukashevich on 28.07.16.
//  Copyright © 2016 Maks Lukashevich. All rights reserved.
//

#import "LTTrackerViewController.h"
#import "LTLocationManger.h"
#import "LTMapViewController.h"

const NSInteger LTMinSpeedValue = 9;

@interface LTTrackerViewController () <LTLocationMangerDelegate>

@property (nonatomic, strong) LTLocationManger * locationManger;
@property (nonatomic, weak) IBOutlet UILabel * speedLabel;
@property (nonatomic, weak) IBOutlet UIButton * startButton;

@property (nonatomic, assign) BOOL movementStarted;
@property (nonatomic, assign) NSUInteger currentSpeed;

@end

@implementation LTTrackerViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  self.title = @"Location tracker";
  self.locationManger = [LTLocationManger sharedInstance];
  self.locationManger.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  [self.locationManger startLocationUpdates];
  [self updateStartButtonState];
}

#pragma mark - Actions

- (IBAction)startAction:(id)sender {
  [self.locationManger startLocationUpdates];
  [self updateStartButtonState];
}

- (IBAction)stop:(id)sender {
  [self.locationManger stopLocationUpdates];
  [self presentRouteController];
}

#pragma mark - Private

- (void)updateStartButtonState {
  self.startButton.hidden = [self.locationManger updatingLocation];
  self.speedLabel.hidden = ![self.locationManger updatingLocation];
}

- (void)presentRouteController {
  LTMapViewController * vc = [LTMapViewController new];
  [vc setLocations:self.locationManger.locations];
  UINavigationController * navController = [[UINavigationController alloc] initWithRootViewController:vc];
  [self presentViewController:navController animated:YES completion:nil];
}

#pragma mark - LTLocationMangerDelegate

- (void)speedChanged:(double)speed {
  self.currentSpeed = (NSUInteger)roundl(speed);
  
  if (self.currentSpeed >= LTMinSpeedValue) {
    self.movementStarted = YES;
    self.speedLabel.hidden = NO;
    [self.locationManger beginLocationsSaving];
  }
  else if (self.currentSpeed <= LTMinSpeedValue && self.movementStarted) {
    self.speedLabel.hidden = YES;
    self.movementStarted = NO;
    [self.locationManger stopLocationUpdates];
    [self presentRouteController];
  }
  self.speedLabel.text = [NSString stringWithFormat:@"%li km/h", (long)self.currentSpeed];
}

- (void)handleError:(NSError *)error {
  [self.locationManger stopLocationUpdates];
  [self updateStartButtonState];
  [self presentError:error];
}

- (void)presentError:(NSError *)error {
  [self alertWithTitle:error.localizedFailureReason message:error.localizedDescription];
}

- (void)alertWithTitle:(NSString *)title message:(NSString *)message {
  if (self.presentedViewController == nil) {
    UIAlertController * controller = [UIAlertController alertControllerWithTitle:title
                                                                         message:message
                                                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * ok = [UIAlertAction actionWithTitle:@"OK"
                                                  style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * _Nonnull action) {
                                                  [controller dismissViewControllerAnimated:YES completion:nil];
                                                }];
    [controller addAction:ok];
    [self presentViewController:controller animated:YES completion:nil];
  }
}
@end
